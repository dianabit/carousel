import React from 'react';
import styled, {css} from 'styled-components';

const ArrowSvgWrapper = styled.div`
	background-color: rgba(128,128,128,0.6);
	width: 55px;
	height: 110px;
	border-left: 0;
	position: absolute;
	top: 30%;
	${props => props.isPrev ? css`
		border-bottom-right-radius: 100px;
		border-top-right-radius: 100px;
		left: 0;
	` : css`
		border-bottom-left-radius: 100px;
		border-top-left-radius: 100px;
		right: 0;
	`}
	&:hover {
    cursor: pointer;
	}
	&:focus {
		outline: none;
	}
`

function Arrow({isPrev, isNext, onClick, nextArrowStyle, prevArrowStyle}) {
	return (
		<ArrowSvgWrapper onClick={onClick} isPrev={isPrev} isNext={isNext}>
		{isNext && (
			<svg 
			version="1.1"
			id="Layer_1" 
			xmlns="http://www.w3.org/2000/svg"
			xlink="http://www.w3.org/1999/xlink"
			x="0px"
			y="0px"
			width="37.6px" 
			height="62.6px" 
			viewBox="0 0 37.6 62.6"
			space="preserve"
			style={nextArrowStyle}
			>
				<g>
					<path 
					d="M3.5,2.1C13.3,12,23.1,20,32.8,29.9C23.1,39.9,13.3,48,3.5,58.1c0-5,0-8,0-13c5.3-4.8,10.6-9.5,16.6-14.9
						c-6-5.5-11.3-10.3-16.6-15.1C3.5,10.1,3.5,7.1,3.5,2.1z"
					/>
				</g>
			</svg>
		)}
		{isPrev && (
			<svg 
				version="1.0" 
				xmlns="http://www.w3.org/2000/svg"
				width="37.6px" 
				height="62.6px"
				viewBox="0 0 38.000000 62.000000"
				preserveAspectRatio="xMidYMid meet"
				style={prevArrowStyle}
			>
		 		<g 
		 			transform="translate(0.000000,62.000000) scale(0.100000,-0.100000)"
					 fill="#000000"
					 stroke="none"
				>
					<path 
							d="M190 460 l-145 -140 145 -140 c80 -77 148 -140 153 -140 4 0 7 30 7
							 68 l0 67 -80 70 -80 70 80 75 80 75 0 68 c0 37 -3 67 -7 67 -5 0 -73 -63 -153-140z"
					/>
				</g>
		 </svg>
		)}
		</ArrowSvgWrapper>
	)
};

export default Arrow;
