import React from 'react';
import styled, {css} from 'styled-components';

const StyledButton = styled.button`
width: 60px;
height: 35px;
margin: 0 5px;
background-color: #ff383f;
color: #fff;
border: 1px solid #fff;
${props => props.isPrev ? css`
  border-bottom-left-radius: 10px;
  border-top-left-radius: 10px;
  ` : css`
  border-bottom-right-radius: 10px;
  border-top-right-radius: 10px;
`}
&:hover {
  cursor: pointer;
  background-color: #ff383f7a;
  color: black;
}
&:focus {
  outline:0;
}
`

function Button(props) {
  const { buttonText, isPrev, onClick } = props;

  return (
    <StyledButton isPrev={isPrev} onClick={onClick}>
      {buttonText}
    </StyledButton>
  )
}

export default Button;