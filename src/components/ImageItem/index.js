import React from 'react';
import styled, {css} from 'styled-components';

const Image = styled.img`
  width: 100vw;
  height: 80vw;
`

function ImageItem(props) {
  const { imageSrc, className } = props;

  return (
    <Image src={imageSrc} className={className} />
  )
}

export default ImageItem;