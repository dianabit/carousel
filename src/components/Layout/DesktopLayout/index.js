import React from 'react';
import styled, {css} from 'styled-components';
import ImageItem from '../../ImageItem';
import User from '../../User';
import Likes from '../../Likes';
import Button from '../../Button';

const DesktopItemsWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
`
const ImagesWrapper = styled.div`
  height: 183px;
  width: 1320px;
  display: flex;
  margin-top: -30vw;
`

const StyledImageItem = styled(ImageItem)`
  width: ${props => props.isActive ? '16vw' : '14vw'};
  height: ${props => props.isActive ? '12vw' : '10vw'};
  ${props => props.isActive && css`
    box-shadow: 0 10px 6px -6px #777;
  `}
`
const Image = styled.div`
  padding: 12px;
`
const Info = styled.div`
  display: flex;
  justify-content: space-between;
`
const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 5vw;
`

function DesktopLayout(props) {
  const { largeImageURL, user, likes, onPrevBtnClick, onNextBtnClick, currentImageIndex } = props;
  return (
    <DesktopItemsWrapper>
      <ImagesWrapper>
        {largeImageURL.map((image, k) => (
          <Image key={k}>
            <StyledImageItem imageSrc={image} isActive={currentImageIndex === k} />
            <Info>
              <User user={user[k]} />
              <Likes likes={likes[k]} />
            </Info>
          </Image>
        ))}
      </ImagesWrapper>
      <ButtonsWrapper>
          <Button isPrev buttonText="Prev" onClick={onPrevBtnClick} />
          <Button buttonText="Next" onClick={onNextBtnClick} />
      </ButtonsWrapper>
  </DesktopItemsWrapper> 
  )
}

export default DesktopLayout;