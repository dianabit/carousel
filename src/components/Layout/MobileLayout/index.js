import React from 'react';
import styled from 'styled-components';
import User from '../../User';
import Likes from '../../Likes';
import ImageItem from '../../ImageItem';
import Arrow from '../../Arrow';

const MbileItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
`
const InfoWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  font-size: larger;
  margin: 5vw 0;
`
const nextArrowStyle = {
  position: 'absolute',
  top: '23%',
  left: '30%'
}

const prevArrowStyle = {
  position: 'absolute',
  top: '23%',
  right: '33%'
}

function MobileLayout(props) {
  const { largeImageURL, onPrevArrowClick, onNextArrowClick, user, likes, currentImageIndex } = props;
  return (
    <MbileItemWrapper>
      <Arrow isPrev onClick={onPrevArrowClick} prevArrowStyle={prevArrowStyle} />
      <ImageItem imageSrc={largeImageURL[currentImageIndex]} />
      <Arrow isNext onClick={onNextArrowClick} nextArrowStyle={nextArrowStyle} />
      <InfoWrapper>
        <User user={user[currentImageIndex]} />
        <Likes likes={likes[currentImageIndex]} />
      </InfoWrapper>
    </MbileItemWrapper>      
  )
}

export default MobileLayout;