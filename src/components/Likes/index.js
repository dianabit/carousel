import React from 'react';
import styled from 'styled-components';

const StyledLikes = styled.div`
  font-size: 14px;
`

function Likes(props) {
  const { likes } = props;

  return (
    <StyledLikes>
      {likes} likes
    </StyledLikes>
  )
}

export default Likes;