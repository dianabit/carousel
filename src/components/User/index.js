import React from 'react';
import styled from 'styled-components';

const StyledUser = styled.div`
  font-size: 14px;
`

function User(props) {
  const { user } = props;
  return (
    <StyledUser>
      Credits to: {user}
    </StyledUser>
  )
}

export default User;