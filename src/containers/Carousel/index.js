import React from 'react';
import styled from 'styled-components';
import MobileLayout from '../../components/Layout/MobileLayout';
import DesktopLayout from '../../components/Layout/DesktopLayout';

const Wrapper = styled.div`
  font-family: 'Ropa Sans', sans-serif;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  height: ${props => props.isMobile ? '100vh' : '790px'};
`

const Title = styled.div`
  display: flex;
  font-size: xx-large;
  font-weight: bold;
`

const ImagesContainer = styled.div`
  display: flex;
  align-items: center;
  height: auto;
`

class Carousel extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      largeImageURL: [],
      user: [],
      likes: []
    }
  }
  
  componentDidMount () {
    const currentStateArray = Object.keys(this.state);
    const { arrayLength } = this.props;
    fetch('https://pixabay.com/api/?key=9656065-a4094594c34f9ac14c7fc4c39')
      .then(response => response.json())
      // Each property that is included in the component state coincides with the property name of each item in the hits array, so the method is generic and might be easily used for any other data in the API 
      // This solution is efficient if there is a large amount of data that must be fetched from the API, we just add to the state the property name
      // But has the disadvantage that if a property name is changed in the API, this method is not liable anymore
      .then(data => {
        currentStateArray.map(key => this.setState({
          [key]: data.hits.map(item => item[key]).slice(0, arrayLength)
        })
      );
  });
}
  
  render() {
    // Not a scalable solution, we will need to refresh the page with the new resolution in order to get the new request and get the userAgent
    // But it is a fast solution that does not require third parties plugins
    const isMobile = /iPhone|iPad|iPod|Android|webOS|BlackBerry|Windows Phone/i.test(navigator.userAgent);
    
    const { largeImageURL, user, likes } = this.state;
    const { 
      onPrevArrowClick, onNextArrowClick, currentImageIndex, onPrevBtnClick, onNextBtnClick, isActive
     } = this.props;
    return (  
      <Wrapper isMobile={isMobile}>
        <Title>Generic Carousel</Title>
        <ImagesContainer>
          {isMobile ? (
            <MobileLayout 
              largeImageURL={largeImageURL}
              onPrevArrowClick={onPrevArrowClick}
              onNextArrowClick={onNextArrowClick}
              user={user}
              likes={likes}
              currentImageIndex={currentImageIndex}
            />
          ) : (
            <DesktopLayout
              currentImageIndex={currentImageIndex}
              isActive={isActive}
              largeImageURL={largeImageURL}
              onNextBtnClick={onNextBtnClick}
              onPrevBtnClick={onPrevBtnClick}
              user={user}
              likes={likes}
            />
          )}
        </ImagesContainer>        
      </Wrapper>
    )
  }  
                 
}

export default Carousel;