import React from 'react';
import Page from '../index';
import { shallow } from 'enzyme';
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

const wantedImageIndex = 1;

it('should increase index when clicking on the next button/arrow', () => {
  // arrange
  const wrapper = shallow(<Page />);
  wrapper.state().currentImageIndex = wantedImageIndex;
  // act
  wrapper.instance().onNextArrowClick();
  // assert
  expect(wrapper.state().currentImageIndex).toBe(wantedImageIndex + 1);
});

it('should decrease index when clicking on the previous button/arrow', () => {
  // arrange
  const wrapper = shallow(<Page />);
  wrapper.state().currentImageIndex = wantedImageIndex;
  // act
  wrapper.instance().onPrevArrowClick();
  // assert
  expect(wrapper.state().currentImageIndex).toBe(wantedImageIndex - 1);
});