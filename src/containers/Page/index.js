import React from 'react';
import Carousel from '../Carousel';

class Page extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      currentImageIndex: 0,
      isActive: false
    }

    this.onPrevArrowClick = this.onPrevArrowClick.bind(this);  
    this.onNextArrowClick = this.onNextArrowClick.bind(this);  
  }

  onPrevArrowClick() {
    const { currentImageIndex } = this.state;
    const lastImageIndex = 5;
    const shouldResetIndex = currentImageIndex === 0;
    const index =  shouldResetIndex ? lastImageIndex : currentImageIndex - 1;
    this.setState({
      currentImageIndex: index
    })
  }

  onNextArrowClick() {
    const { currentImageIndex } = this.state;
    const lastImageIndex = 5;
    const shouldResetIndex = currentImageIndex === lastImageIndex;
    const index =  shouldResetIndex ? 0 : currentImageIndex + 1;
    this.setState({
      currentImageIndex: index,
      isActive: true
    });
  }
  
  render() {
    const { currentImageIndex, isActive } = this.state;

    return (  
      <Carousel 
        onPrevArrowClick={this.onPrevArrowClick}
        onNextArrowClick={this.onNextArrowClick}
        onPrevBtnClick={this.onPrevArrowClick}
        onNextBtnClick={this.onNextArrowClick}
        currentImageIndex={currentImageIndex}
        isActive={isActive}
        arrayLength={6}
      />
    )
  }  
                 
}

export default Page;