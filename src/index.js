import React from 'react';
import ReactDOM from "react-dom";
import Page from './containers/Page/index';

ReactDOM.hydrate(
<Page />,
document.getElementById('app'));